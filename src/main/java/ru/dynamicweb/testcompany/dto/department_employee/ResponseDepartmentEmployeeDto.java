package ru.dynamicweb.testcompany.dto.department_employee;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDepartmentEmployeeDto {
    private Long departmentId;
    private Long employeeId;
    private String employeePosition;

    public  ResponseDepartmentEmployeeDto(Long departmentId, Long employeeId, String employeePosition){
        this.departmentId = departmentId;
        this.employeeId = employeeId;
        this.employeePosition = employeePosition;
    }
}
