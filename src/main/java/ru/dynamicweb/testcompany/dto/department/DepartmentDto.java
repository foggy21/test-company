package ru.dynamicweb.testcompany.dto.department;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.dynamicweb.testcompany.dto.employee.EmployeeDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DepartmentDto {
    private Long id;
    private String name;
    private Long mainDepartmentId;
    private List<EmployeeDto> employees;
    private List<DepartmentDto> divisions;

}
