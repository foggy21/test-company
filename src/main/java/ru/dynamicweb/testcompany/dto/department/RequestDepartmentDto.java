package ru.dynamicweb.testcompany.dto.department;

import lombok.Getter;

@Getter
public class RequestDepartmentDto {
    private String name;
}
