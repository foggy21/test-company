package ru.dynamicweb.testcompany.dto.department;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponseDepartmentDto {
    private Long id;
    private String name;
    private Long parentDepartmentId;

    public ResponseDepartmentDto(Long id, String name){
        this.id = id;
        this.name = name;
        this.parentDepartmentId = null;
    }

    public ResponseDepartmentDto(Long id, String name, Long parentDepartmentId){
        this.id = id;
        this.name = name;
        this.parentDepartmentId = parentDepartmentId;
    }
}
