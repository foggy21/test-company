package ru.dynamicweb.testcompany.dto.employee;

import lombok.Getter;

@Getter
public class RequestEmployeeDto {
    private String name;
}
