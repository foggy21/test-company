package ru.dynamicweb.testcompany.dto.employee;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResponseEmployeeDto {
    private Long id;
    private String name;

    public ResponseEmployeeDto(Long id, String name){
        this.id = id;
        this.name = name;
    }
}
