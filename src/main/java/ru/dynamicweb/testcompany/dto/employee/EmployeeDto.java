package ru.dynamicweb.testcompany.dto.employee;

import lombok.Getter;
import lombok.Setter;
import ru.dynamicweb.testcompany.dto.department.DepartmentDto;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class EmployeeDto {
    private Long id;
    private String name;
    private String position;

    public EmployeeDto(Long id, String name, String position){
        this.id = id;
        this.name = name;
        this.position = position;
    }
}
