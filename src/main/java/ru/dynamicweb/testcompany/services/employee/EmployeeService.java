package ru.dynamicweb.testcompany.services.employee;

import ru.dynamicweb.testcompany.dto.department.DepartmentDto;
import ru.dynamicweb.testcompany.dto.employee.EmployeeDto;
import ru.dynamicweb.testcompany.models.Employee;

import java.util.List;

public interface EmployeeService {
    Employee saveEmployee(Employee employee);
    List<EmployeeDto> getEmployeesDtoByDepartmentId(Long departmentId);
    void findEmployeesInDepartments(List<DepartmentDto> departmentsDto);
}
