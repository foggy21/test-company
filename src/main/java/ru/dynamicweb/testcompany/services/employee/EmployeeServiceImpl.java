package ru.dynamicweb.testcompany.services.employee;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dynamicweb.testcompany.dto.department.DepartmentDto;
import ru.dynamicweb.testcompany.dto.employee.EmployeeDto;
import ru.dynamicweb.testcompany.models.Department;
import ru.dynamicweb.testcompany.models.DepartmentEmployee;
import ru.dynamicweb.testcompany.models.Employee;
import ru.dynamicweb.testcompany.repositories.department_employee.DepartmentEmployeeRepository;
import ru.dynamicweb.testcompany.repositories.employee.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class EmployeeServiceImpl implements EmployeeService {

    private final DepartmentEmployeeRepository departmentEmployeeRepository;
    private final EmployeeRepository employeeRepository;

    @Override
    public Employee saveEmployee(Employee employee) {
        String nameOfEmployee = employee.getName();
        if (nameOfEmployee == null) {
            throw new IllegalArgumentException("name mustn't be null");
        }
        return employeeRepository.save(employee);
    }

    @Override
    public List<EmployeeDto> getEmployeesDtoByDepartmentId(Long departmentId) {
        List<DepartmentEmployee> departmentEmployees = departmentEmployeeRepository.findByDepartmentId(departmentId);
        List<EmployeeDto> employeesDto = new ArrayList<>();
        for(DepartmentEmployee departmentEmployee : departmentEmployees){
            EmployeeDto employeeDto = new EmployeeDto(
                    departmentEmployee.getId().getEmployeeId(),
                    departmentEmployee.getEmployee().getName(),
                    departmentEmployee.getEmployeePosition()
            );
            employeesDto.add(employeeDto);
        }
        return employeesDto;
    }

    @Override
    public void findEmployeesInDepartments(List<DepartmentDto> departmentsDto) {
        if (departmentsDto.size() == 0){
            return;
        }
        for (DepartmentDto departmentDto : departmentsDto){
            List<EmployeeDto> employeesDto = getEmployeesDtoByDepartmentId(departmentDto.getId());
            findEmployeesInDepartments(departmentDto.getDivisions());
            departmentDto.setEmployees(employeesDto);
        }
    }
}
