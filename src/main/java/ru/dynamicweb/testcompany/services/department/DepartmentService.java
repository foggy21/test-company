package ru.dynamicweb.testcompany.services.department;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import ru.dynamicweb.testcompany.dto.department.DepartmentDto;
import ru.dynamicweb.testcompany.models.Department;
import ru.dynamicweb.testcompany.models.DepartmentEmployee;

import java.util.List;

public interface DepartmentService {
    Department saveDepartment(Department department);

    Department saveDivisionForMainDepartment(Department department, Long mainDepartmentId);

    DepartmentEmployee addEmployeeToDepartment(Long departmentId, Long employeeId, String employeePosition);

    List<DepartmentDto> getMainDepartments();
    void findDivisionsInDepartments(List<DepartmentDto> departmentsDto);

    List<DepartmentDto> getDivisionsDtoByDepartmentId(Long departmentId);

}
