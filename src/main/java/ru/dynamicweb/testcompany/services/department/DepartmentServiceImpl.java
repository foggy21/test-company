package ru.dynamicweb.testcompany.services.department;

import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.dynamicweb.testcompany.dto.department.DepartmentDto;
import ru.dynamicweb.testcompany.models.Department;
import ru.dynamicweb.testcompany.models.DepartmentEmployee;
import ru.dynamicweb.testcompany.models.Employee;
import ru.dynamicweb.testcompany.repositories.department.DepartmentRepository;
import ru.dynamicweb.testcompany.repositories.department_employee.DepartmentEmployeeRepository;
import ru.dynamicweb.testcompany.repositories.employee.EmployeeRepository;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class DepartmentServiceImpl implements DepartmentService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final DepartmentEmployeeRepository departmentEmployeeRepository;

    @Override
    public Department saveDepartment(Department department) {
        String nameOfDepartment = department.getName();
        if (nameOfDepartment == null) {
            throw new IllegalArgumentException("name of department mustn't be null");
        }
        return departmentRepository.save(department);
    }

    @Override
    public Department saveDivisionForMainDepartment(Department division, Long mainDepartmentId) {
        String nameOfDepartment = division.getName();
        if (nameOfDepartment == null || mainDepartmentId == null) {
            throw new IllegalArgumentException("arguments mustn't be null");
        }
        Department mainDepartment = departmentRepository.findById(mainDepartmentId).orElseThrow(
                () -> new EntityNotFoundException("Department with this id " + mainDepartmentId + " not found.")
        );
        mainDepartment.getDivisions().add(division);
        division.setMainDepartment(mainDepartment);
        return departmentRepository.save(division);
    }

    @Override
    public DepartmentEmployee addEmployeeToDepartment(Long departmentId, Long employeeId, String employeePosition) {
        if (employeeId == null || departmentId == null || employeePosition == null) {
            throw new IllegalArgumentException("arguments mustn't be null");
        }
        Employee employee = employeeRepository.findById(employeeId).orElseThrow(
                () -> new EntityNotFoundException("Employee with this id " + employeeId + " not found.")
        );
        Department department = departmentRepository.findById(departmentId).orElseThrow(
                () -> new EntityNotFoundException("Department with this id " + departmentId + " not found.")
        );
        DepartmentEmployee departmentEmployee = new DepartmentEmployee(department, employee, employeePosition);
        department.getEmployees().add(departmentEmployee);
        employee.getDepartments().add(departmentEmployee);
        departmentRepository.save(department);
        employeeRepository.save(employee);
        return departmentEmployeeRepository.save(departmentEmployee);
    }

    @Override
    public List<DepartmentDto> getMainDepartments() {
        List<Department> mainDepartments = departmentRepository.findAllByMainDepartmentIdIsNull();
        List<DepartmentDto> mainDepartmentsDto = new ArrayList<>();
        for (Department mainDepartment : mainDepartments){
            DepartmentDto mainDepartmentDto = new DepartmentDto();
            mainDepartmentDto.setId(mainDepartment.getId());
            mainDepartmentDto.setName(mainDepartment.getName());
            mainDepartmentDto.setMainDepartmentId(null);
            mainDepartmentsDto.add(mainDepartmentDto);
        }
        return mainDepartmentsDto;
    }

    @Override
    public void findDivisionsInDepartments(List<DepartmentDto> departmentsDto) {
        if (departmentsDto.size() == 0){
            return;
        }
        for (DepartmentDto departmentDto : departmentsDto){
            List<DepartmentDto> divisionsDto = getDivisionsDtoByDepartmentId(departmentDto.getId());
            findDivisionsInDepartments(divisionsDto);
            departmentDto.setDivisions(divisionsDto);
        }
    }

    @Override
    public List<DepartmentDto> getDivisionsDtoByDepartmentId(Long departmentId) {
        List<Department> departments = departmentRepository.findAllByMainDepartmentId(departmentId);
        List<DepartmentDto> divisionsDto = new ArrayList<>();
        for (Department department : departments){
            DepartmentDto division = new DepartmentDto();
            division.setId(department.getId());
            division.setName(department.getName());
            division.setMainDepartmentId(departmentId);
            divisionsDto.add(division);
        }
        return divisionsDto;
    }


}
