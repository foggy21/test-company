package ru.dynamicweb.testcompany.repositories.department_employee;

import ru.dynamicweb.testcompany.models.DepartmentEmployee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DepartmentEmployeeRepository extends JpaRepository<DepartmentEmployee, Long> {
    List<DepartmentEmployee> findByDepartmentId(Long departmentId);
}
