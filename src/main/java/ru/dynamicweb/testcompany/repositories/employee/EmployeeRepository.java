package ru.dynamicweb.testcompany.repositories.employee;

import ru.dynamicweb.testcompany.models.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
