package ru.dynamicweb.testcompany.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "department")
@Getter
@Setter
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "department_name")
    private String name;

    @OneToMany(mappedBy = "department",
            cascade = CascadeType.MERGE,
            orphanRemoval = true)
    @JsonIgnore
    List<DepartmentEmployee> employees = new ArrayList<>();

    @ManyToOne
    private Department mainDepartment;

    @OneToMany(mappedBy = "mainDepartment",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JsonIgnore
    List<Department> divisions = new ArrayList<>();
}
