package ru.dynamicweb.testcompany.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Data
@Entity
@Table(name = "departments_employees")
public class DepartmentEmployee {

    @EmbeddedId
    private DepartmentEmployeeId id;

    @ManyToOne
    @MapsId("departmentId")
    @JoinColumn(name = "department_id")
    private Department department;

    @ManyToOne
    @MapsId("employeeId")
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @Column(name = "employee_position")
    private String employeePosition;

    public DepartmentEmployee(Department department, Employee employee, String employeePosition){
        this.id = new DepartmentEmployeeId(department.getId(), employee.getId());
        this.department = department;
        this.employee = employee;
        this.employeePosition = employeePosition;
    }
}
