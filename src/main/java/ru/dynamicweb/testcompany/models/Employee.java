package ru.dynamicweb.testcompany.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "employees")
@Data
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "employee_name")
    private String name;

    @OneToMany(mappedBy = "employee",
            cascade = CascadeType.MERGE,
            orphanRemoval = true)
    @JsonIgnore
    private List<DepartmentEmployee> departments = new ArrayList<>();
}
