package ru.dynamicweb.testcompany.controllers;

import jakarta.persistence.EntityNotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.dynamicweb.testcompany.dto.department.DepartmentDto;
import ru.dynamicweb.testcompany.dto.department.RequestDepartmentDto;
import ru.dynamicweb.testcompany.dto.department.ResponseDepartmentDto;
import ru.dynamicweb.testcompany.dto.department_employee.ResponseDepartmentEmployeeDto;
import ru.dynamicweb.testcompany.models.Department;
import ru.dynamicweb.testcompany.services.department.DepartmentService;
import ru.dynamicweb.testcompany.services.employee.EmployeeService;

import java.util.List;

@AllArgsConstructor
@RequestMapping("/department")
@CrossOrigin
@RestController
public class DepartmentController {

    private final EmployeeService employeeService;
    private final DepartmentService departmentService;

    @PostMapping
    public ResponseDepartmentDto saveDepartment(@RequestBody RequestDepartmentDto requestDepartmentDto) {
        Department department = new Department();
        department.setName(requestDepartmentDto.getName());
        Department savedDepartment = departmentService.saveDepartment(department);
        var responseDepartmentDto = new ResponseDepartmentDto(
                savedDepartment.getId(),
                savedDepartment.getName()
        );
        return responseDepartmentDto;
    }

    @PostMapping("/{id}/division")
    public ResponseDepartmentDto saveDivision(@RequestBody RequestDepartmentDto requestDivisionDto,
                                                   @PathVariable(name = "id") Long mainDepartmentId) {
        Department division = new Department();
        division.setName(requestDivisionDto.getName());
        Department savedDivision = departmentService.saveDivisionForMainDepartment(division, mainDepartmentId);
        var responseDepartmentDto = new ResponseDepartmentDto(
                savedDivision.getId(),
                savedDivision.getName(),
                savedDivision.getMainDepartment().getId()
        );
        return responseDepartmentDto;
    }

    @PostMapping("/{departmentId}/employee/{employeeId}")
    public ResponseDepartmentEmployeeDto saveEmployeeInDepartment(@PathVariable(name = "departmentId") Long departmentId,
                                                                  @PathVariable(name = "employeeId") Long employeeId,
                                                                  @RequestBody String employeePosition){
        var departmentEmployee = departmentService.addEmployeeToDepartment(departmentId, employeeId, employeePosition);
        var responseDepartmentEmployeeDto = new ResponseDepartmentEmployeeDto(
                departmentEmployee.getId().getDepartmentId(),
                departmentEmployee.getId().getEmployeeId(),
                departmentEmployee.getEmployeePosition()
        );
        return responseDepartmentEmployeeDto;
    }

    @GetMapping("/hierarchy")
    public List<DepartmentDto> getHierarchy() {
        List<DepartmentDto> departmentsDto = departmentService.getMainDepartments();
        if (departmentsDto.size() == 0) {
            throw new EntityNotFoundException("Not found main departments");
        }
        departmentService.findDivisionsInDepartments(departmentsDto);
        employeeService.findEmployeesInDepartments(departmentsDto);
        return departmentsDto;
    }
}
