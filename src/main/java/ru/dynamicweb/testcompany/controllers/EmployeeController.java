package ru.dynamicweb.testcompany.controllers;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.dynamicweb.testcompany.dto.employee.RequestEmployeeDto;
import ru.dynamicweb.testcompany.dto.employee.ResponseEmployeeDto;
import ru.dynamicweb.testcompany.models.Employee;
import ru.dynamicweb.testcompany.services.employee.EmployeeService;

@AllArgsConstructor
@RequestMapping("/employee")
@CrossOrigin
@RestController
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    public ResponseEmployeeDto saveEmployee(@RequestBody RequestEmployeeDto requestEmployeeDto){
        Employee employee = new Employee();
        employee.setName(requestEmployeeDto.getName());
        Employee savedEmployee = employeeService.saveEmployee(employee);
        var responseEmployeeDto = new ResponseEmployeeDto(
                savedEmployee.getId(),
                savedEmployee.getName()
        );
        return responseEmployeeDto;

    }
}
